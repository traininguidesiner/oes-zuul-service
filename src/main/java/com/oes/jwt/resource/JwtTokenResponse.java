package com.oes.jwt.resource;

import java.io.Serializable;

public class JwtTokenResponse implements Serializable {

	private static final long serialVersionUID = 8317676219297719109L;

	private final String token;

	private  Integer id;
	
	private String userType;
	
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public JwtTokenResponse(String token) {
		this.token = token;
	}

	public JwtTokenResponse(String token, Integer id, String userType) {
		super();
		this.token = token;
		this.id = id;
		this.userType = userType;
	}

	
}
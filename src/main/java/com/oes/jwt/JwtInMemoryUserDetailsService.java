package com.oes.jwt;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.oes.administration.model.AdminUserRegistrationModel;
import com.oes.administration.service.AdminUserRegistrationService;
import com.oes.user.model.UserRegistrationDetailsModel;
import com.oes.user.service.UserRegistrationDetailsService;

@Service
public class JwtInMemoryUserDetailsService implements UserDetailsService {

	@Autowired
	UserRegistrationDetailsService userRegistrationDetailsService;
	@Autowired
	 AdminUserRegistrationService adminUserRegistrationService;
	static List<JwtUserDetails> inMemoryUserList = new ArrayList<>();

	static {
		inMemoryUserList.add(new JwtUserDetails(1, "in28minutes",
				"$2a$10$3zHzb.Npv1hfZbLEU5qsdOju/tk2je6W6PnNnY.c1ujWPcZh4PL6e", "ROLE_USER_2"));
		inMemoryUserList.add(new JwtUserDetails(2, "ranga",
				"$2a$10$IetbreuU5KihCkDB6/r1DOJO0VyU9lSiBcrMDT.biU7FOt2oqZDPm", "ROLE_USER_2"));
		
		//$2a$10$IetbreuU5KihCkDB6/r1DOJO0VyU9lSiBcrMDT.biU7FOt2oqZDPm
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UserRegistrationDetailsModel usersList = userRegistrationDetailsService.findUserByUserName(username);
		AdminUserRegistrationModel admin = adminUserRegistrationService.findAdminByName(username);
		
		List<JwtUserDetails> inMemoryUserList = new ArrayList<>();
				
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String email = null;
		Integer id = null;
		String password = null;
		String role = null;
		if(Objects.nonNull(usersList)) {
			id = usersList.getUserRegistrationId();
			email = usersList.getEmail();
			password = usersList.getPassword();
			role = "user";
		}
		else if(Objects.nonNull(admin)) {
			id = admin.getAdminId();
			email = admin.getEmail();
			password = admin.getPassword();
			role = "admin";
		}
		inMemoryUserList.add(
				new JwtUserDetails(
						id,
						email,
						encoder.encode(password),
						role));
		Optional<JwtUserDetails> findFirst = inMemoryUserList.stream().filter(user -> user.getUsername().equals(username)).findFirst();
		
		//System.out.println(usersList.toString());
		if (!findFirst.isPresent()) {
			throw new UsernameNotFoundException(String.format("USER_NOT_FOUND '%s'.", username));
		}

		return findFirst.get();
	}

}

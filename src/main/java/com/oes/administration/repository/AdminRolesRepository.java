package com.oes.administration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oes.administration.model.AdminRolesModel;

public interface AdminRolesRepository extends JpaRepository<AdminRolesModel, Integer>
{

	AdminRolesModel findByAdminRoleName(String adminRoleName);

	//@Query("select r from admin_roles r where r.role_name=:userName")
	//List<AdminRolesModel> findByRoleName(@Param("userName") String userName);

}

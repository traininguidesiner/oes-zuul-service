package com.oes.administration.service;

import java.util.List;

import com.oes.administration.model.AdminUserRegistrationModel;

public interface AdminUserRegistrationService
{
	public AdminUserRegistrationModel saveAdmin(AdminUserRegistrationModel adminDetails);
	
	public AdminUserRegistrationModel updateAdmin(AdminUserRegistrationModel adminDetails);
	
	public void deleteAdmin(AdminUserRegistrationModel adminDetails);
	
	public AdminUserRegistrationModel findAdminById(Integer adminId);
	
	public AdminUserRegistrationModel findAdminByName(String adminUname);
	
	public AdminUserRegistrationModel findAdminByEmailAndPwd(String adminUname, String password);
	
	public boolean getAll(String value);
	//public List<AdminUserRegistrationModel> findAdminAll();
	
	public void updateAdminRole(Integer roleId, Integer adminId);

	public List<AdminUserRegistrationModel> getAllAdminUser();
	

}

package com.oes.administration.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oes.administration.model.AdminUserRegistrationModel;
import com.oes.administration.repository.AdminUserRegistrationRepository;
import com.oes.administration.service.AdminUserRegistrationService;
import com.oes.exception.OESException;
import com.oes.user.model.UserRegistrationDetailsModel;

@Service
public class AdminUserRegistrationServiceImpl implements AdminUserRegistrationService 
{
	@Autowired
	AdminUserRegistrationRepository repo;

	@Override
	public AdminUserRegistrationModel saveAdmin(AdminUserRegistrationModel adminDetails) {
		return repo.save(adminDetails);
	}

	@Override
	public AdminUserRegistrationModel updateAdmin(AdminUserRegistrationModel adminDetails)
	{
		if(Objects.isNull(adminDetails.getAdminId()))
			{
				throw new OESException("invalid update request", HttpStatus.BAD_REQUEST);
			}
		return repo.save(adminDetails);
	}

	@Override
	public void deleteAdmin(AdminUserRegistrationModel adminDetails)
	{
		repo.delete(adminDetails);
	}

	@Override
	public AdminUserRegistrationModel findAdminById(Integer adminId)
	{
		return repo.findById(adminId).get();
	}
	
	@Override
	public AdminUserRegistrationModel findAdminByName(String adminUname)
	{
		return repo.findByEmail(adminUname);
	}

	@Override
	public AdminUserRegistrationModel findAdminByEmailAndPwd(String adminUname, String password)
	{
		return repo.findByEmailAndPwd(adminUname, password);
	}

	@Override
	public boolean getAll(String value)
	{
		List<AdminUserRegistrationModel> list = repo.findByUserName(value);
		boolean flag = false;
		if(list.size()>0) {
			flag = true;
		}
		return flag;
	}
	
	@Override
	public List<AdminUserRegistrationModel> getAllAdminUser()
	{
		return repo.findAll();
	}

	@Override
	public void updateAdminRole(Integer roleId,Integer adminId) {
		
		repo.updateAdminRoleId(roleId,adminId);
	}

	
}
